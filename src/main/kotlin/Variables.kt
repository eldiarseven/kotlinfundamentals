fun main () {
    // mutable
    var myName = "Lee"
    // immutable
    val mySurname = "Del Rosario"
    println(myName)
    println(mySurname)
    val myAge = 0
    // mySurname = "Sy"
    myName = "Llino"
    println(myName)
    println(myAge)
}